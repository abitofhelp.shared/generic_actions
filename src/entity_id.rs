use bson::oid::ObjectId;
use uuid::Uuid;

use crate::entity_error::{BoxError, EntityError};

// Represents the unique id for an entity in a data store.
//    where T is either UuidType or ObjectIdType.
// Defines two methods that must be implemented:
//    generator() to create a new Uuid or ObjectId.
//    parser() to parse a string slice id and produce either a Uuid or ObjectId.
pub(crate) trait EntityId<T> {
    fn generator() -> T;
    fn parser(id: &str) -> Result<T, EntityError>;
}

// UuidType: Define and implement EntityIdType<Uuid>.
#[derive(Debug)]
pub(crate) struct EntityUuid;

impl EntityId<Uuid> for EntityUuid {
    fn generator() -> Uuid {
        Uuid::new_v4()
    }

    fn parser(id: &str) -> Result<Uuid, EntityError> {
        match Uuid::parse_str(id) {
            Ok(uid) => Ok(uid),
            Err(e) => Err(EntityError::Application {
                message: "failed to parse the uuid".to_owned(),
                err: Some(BoxError::from(e)),
            }),
        }
    }
}

// ObjectIdType: Define and implement EntityIdType<ObjectId>.
#[derive(Debug)]
pub(crate) struct EntityObjectId;

impl EntityId<ObjectId> for EntityObjectId {
    fn generator() -> ObjectId {
        ObjectId::new()
    }

    fn parser(id: &str) -> Result<ObjectId, EntityError> {
        match ObjectId::parse_str(id) {
            Ok(oid) => Ok(oid),
            Err(e) => Err(EntityError::Application {
                message: "failed to parse the objectid".to_owned(),
                err: Some(BoxError::from(e)),
            }),
        }
    }
}

// Create a data storage id as either a Uuid or ObjectId.
//    where T is either UuidType or ObjectIdType; and
//          V is either Uuid     or ObjectId
// id: The unique, string-slice value id for an entity in storage.
// returns: the data store specific id.
pub(crate) fn make_id<T, V>(id: Option<&str>) -> Result<V, EntityError>
    where
        T: EntityId<V>,
{
    id.map_or_else(
        || Ok(T::generator()),
        |pid| match T::parser(pid) {
            Ok(rid) => Ok(rid),
            Err(e) => Err(e),
        },
    )
}

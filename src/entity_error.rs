use std::error::Error;

use thiserror::Error;

use crate::error::ApiError;

pub type BoxError = Box<dyn Error + Send + Sync + 'static>;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum EntityError {
    #[error("application error(code=500): {message}")]
    Application {
        message: String,
        err: Option<BoxError>,
    },

    #[error("request is malformed(code=400): {message}")]
    BadRequest {
        message: String,
        err: Option<BoxError>,
    },

    #[error("request conflicts with an existing resource(code=409): {message}")]
    Conflict {
        message: String,
        err: Option<BoxError>,
    },

    #[error("resource not allowed(code=403): {message}")]
    Forbidden {
        message: String,
        err: Option<BoxError>,
    },

    #[error("an internal server error occurred(code=500): {message}")]
    InternalServerError {
        message: String,
        err: Option<BoxError>,
    },

    #[error("requested resource was not found(code=404): {message}")]
    NotFound {
        message: String,
        err: Option<BoxError>,
    },

    #[error("not authenticated or token expired(code=401): {message}")]
    Unauthorized {
        message: String,
        err: Option<BoxError>,
    },
}

impl From<ApiError> for EntityError {
    fn from(value: ApiError) -> Self {
        match value {
            ApiError::InternalServerError { message, err } => {
                Self::InternalServerError { message, err }
            }
            ApiError::Application { message, err } => Self::Application { message, err },
            ApiError::BadRequest { message, err } => Self::BadRequest { message, err },
            ApiError::Conflict { message, err } => Self::Conflict { message, err },
            ApiError::Forbidden { message, err } => Self::Forbidden { message, err },
            ApiError::NotFound { message, err } => Self::NotFound { message, err },
            ApiError::Unauthorized { message, err } => Self::Unauthorized { message, err },
        }
    }
}

impl From<EntityError> for ApiError {
    fn from(val: EntityError) -> Self {
        match val {
            EntityError::InternalServerError { message, err } => {
                Self::InternalServerError { message, err }
            }
            EntityError::Application { message, err } => Self::Application { message, err },
            EntityError::BadRequest { message, err } => Self::BadRequest { message, err },
            EntityError::Conflict { message, err } => Self::Conflict { message, err },
            EntityError::Forbidden { message, err } => Self::Forbidden { message, err },
            EntityError::NotFound { message, err } => Self::NotFound { message, err },
            EntityError::Unauthorized { message, err } => Self::Unauthorized { message, err },
            // _ => Self::InternalServerError {
            //     message: format!("unknown ApiError enumeration: {val:?}"),
            //     err: None,
            // },
        }
    }
}

// impl Into<ApiError> for EntityError {
//     fn into(self) -> ApiError {
//         match self {
//             Self::InternalServerError { message, err } => {
//                 ApiError::InternalServerError { message, err }
//             }
//             Self::Application { message, err } => ApiError::Application { message, err },
//             Self::BadRequest { message, err } => ApiError::BadRequest { message, err },
//             Self::Conflict { message, err } => ApiError::Conflict { message, err },
//             Self::Forbidden { message, err } => ApiError::Forbidden { message, err },
//             Self::NotFound { message, err } => ApiError::NotFound { message, err },
//             Self::Unauthorized { message, err } => ApiError::Unauthorized { message, err },
//             _ => ApiError::InternalServerError {
//                 message: format!("unknown ApiError enumeration: {self:?}"),
//                 err: None,
//             },
//         }
//     }
// }

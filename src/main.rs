use bson::oid::ObjectId;
use uuid::Uuid;

use crate::entity_id::{EntityObjectId, EntityUuid, make_id};

mod entity_id;
mod entity_error;
mod error;

fn main() {
    let uuid = "142c458f-2728-4961-b4c5-6514c7491691";
    let objectid = "6507fdd16c27fffd69308982";

    let parsed_uuid = make_id::<EntityUuid, Uuid>(Some(uuid));
    let parsed_objectid = make_id::<EntityObjectId, ObjectId>(Some(objectid));

    println!("Parsed UUID '{uuid}' to '{parsed_uuid:?}'");
    println!("Parsed ObjectId '{objectid}' to '{parsed_objectid:?}'");

    let generated_uuid = make_id::<EntityUuid, Uuid>(None);
    let generated_objectid = make_id::<EntityObjectId, ObjectId>(None);

    println!("Generated UUID '{generated_uuid:?}'");
    println!("Generated ObjectId '{generated_objectid:?}'");
}
